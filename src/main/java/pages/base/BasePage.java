package pages.base;

import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import gherkin.lexer.Th;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by logovskoy on 12/3/2015.
 */
public abstract class BasePage extends PageInstance {
    protected int timeOut = 60;

    public BasePage() {
        PageFactory.initElements(driver, this);
    }

    /**
     * @Return Element is waiting for to define page is loaded.
     */
    protected abstract WebElement elementForLoading() throws Exception;

    /**
     * @Return Page class name.
     */
    public String toString() {
        return this.getClass().getSimpleName();
    }

    public void waitForElement(String xpath, int seconds) throws Exception {
        int currentseconds = 0;
        while (true) {
            try {
                driver.findElement(By.xpath(xpath));
                return;
            } catch (Throwable ex) {
                if (currentseconds == seconds) {
                    throw new Exception("No such element in given time");
                }
                Thread.sleep(1000);
                currentseconds++;
            }
        }
    }
}