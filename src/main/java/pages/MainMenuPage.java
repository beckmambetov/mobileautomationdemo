package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.FindBy;
import org.sikuli.api.robot.Key;
import pages.base.BasePage;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by beckmambetov on 9/22/2016.
 */
public class MainMenuPage extends BasePage {
    @Override
    protected WebElement elementForLoading() throws Exception {
        return null;
    }

    @FindBy(xpath = ".//android.widget.TextView[@text='ПРИЛОЖЕНИЯ']")
    private WebElement appsButton;

    @FindBy(xpath = ".//android.widget.TextView[@text='ИГРЫ']")
    private WebElement gamesButton;

    @FindBy(xpath = ".//android.widget.TextView[@text='ФИЛЬМЫ']")
    private WebElement moviesButton;

    @FindBy(xpath = ".//android.widget.TextView[@text='МУЗЫКА']")
    private WebElement musicButton;

    @FindBy(xpath = ".//android.widget.TextView[@text='КНИГИ']")
    private WebElement booksButton;

    @FindBy(xpath = ".//android.widget.TextView[@text='ПРЕССА']")
    private WebElement pressButton;

    @FindBy(xpath = ".//android.widget.EditText[contains(@text,'Что искать')]")
    private WebElement searchTextField;

    @FindBy(xpath = ".//android.support.v7.widget.LinearLayoutCompat[@package='com.android.vending']")
    private WebElement searchButton;



    public boolean isCurrentPage() {
        try {
            Point x = appsButton.getLocation();
            if (x!=null) {
                return true;
            }
        } catch (Throwable ex) {

        }
        return false;
    }

    public void search(String target) throws IOException, InterruptedException {
        long x = new Date().getTime();
        searchButton.click();
        searchTextField.sendKeys(target);
        ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "adb shell input keyevent KEYCODE_ENTER");
        builder.redirectErrorStream(true);
        Process p = builder.start();
        Thread.sleep(3000);
        long y = new Date().getTime();
        System.out.println(y - x);
        p.destroy();
    }

    public boolean checkSearchResults(String target) {
        List<WebElement> results = driver.findElements(By.xpath(".//android.widget.TextView[@resource-id='com.android.vending:id/li_title']"));
        return results.stream().allMatch(p->p.getAttribute("text").contains(target));
    }

    public void goToResult(String name) {
        List<WebElement> results = driver.findElements(By.xpath(".//android.widget.TextView[@resource-id='com.android.vending:id/li_title']"));
        results.stream().filter(p->p.getAttribute("text").equals(name)).collect(Collectors.toList()).get(0).click();
    }


}
