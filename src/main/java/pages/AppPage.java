package pages;

import gherkin.lexer.Th;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.base.BasePage;

/**
 * Created by beckmambetov on 9/28/2016.
 */
public class AppPage extends BasePage{

    @FindBy(xpath = ".//android.widget.Button[@text='УСТАНОВИТЬ']")
    private WebElement installAppButton;

    @FindBy(xpath =".//android.widget.TextView[@text='ПРИНЯТЬ']")
    private WebElement acceptInstallButton;

    @FindBy(xpath = ".//android.widget.Button[@text='ОТКРЫТЬ']")
    private WebElement openAppButton;

    @FindBy(xpath = ".//android.widget.Button[@text='УДАЛИТЬ']")
    private WebElement deleteAppButton;

    @FindBy(xpath = ".//android.widget.Button[@text='ОК']")
    private WebElement okButton;

    @FindBy(xpath = ".//android.widget.TextView[@resource-id='com.android.vending:id/title_title']")
    private WebElement title;

    @FindBy(xpath = ".//android.widget.Button[@text='Продолжить']")
    private WebElement acceptButton;

    @FindBy(xpath = ".//android.widget.TextView[@resource-id='com.android.vending:id/average_value']")
    private WebElement avgMark;

    public void installApp() throws Exception {
        installAppButton.click();
        Thread.sleep(2000);
        acceptInstallButton.click();
        try {
            acceptButton.click();
        } catch (Throwable ex) {

        }
        waitForElement(".//android.widget.Button[@text='УДАЛИТЬ']",300);
    }

    public void deleteApp() {
        deleteAppButton.click();
        okButton.click();
    }

    public boolean isAppInstalled(String name) {
        try {
            return title.getAttribute("text").equals(name) && deleteAppButton.getLocation() != null;
        } catch (Throwable ex) {
            return false;
        }
    }

    public String getAverageMark() {
        return avgMark.getAttribute("text");
    }

    @Override
    protected WebElement elementForLoading() throws Exception {
        return null;
    }
}
