package pages.current;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.base.BasePage;

/**
 * Created by beckmambetov on 5/20/2016.
 */
public class LoginPage extends BasePage {
    @FindBy(xpath = ".//input[id='usernamefield']")
    public WebElement usernameInput;
    @FindBy(xpath = ".//input[id='passwordfield']")
    public WebElement passwordInput;
    @FindBy(xpath = ".//button[name='loginButton']")
    public WebElement loginButton;

    @Override
    protected WebElement elementForLoading() throws Exception {
        return null;
    }


}
