@Feature_123
Feature: Demo

  @High
  Scenario: Search
    Given I`m on a main page
    When I enter "Angry" into search string
    Then I see search results and they contain "Angry" in their name

  @High
  Scenario: Installation and Deinstallation
    Given I`m on a main page
    When I enter "Angry Birds 2" into search string
    And I select "Angry Birds 2" application
    And I install app
    Then I see that app "Angry Birds 2" is "" installed
    And I delete app
    And I see that app "Angry Birds 2" is "not" installed

  @High
  Scenario: Mark check
    Given I`m on a main page
    When I enter "Angry Birds 2" into search string
    And I select "Angry Birds 2" application
    Then I see that it`s average mark is "4,5"




