package testUtils;

import helpers.SystemHelper;
import arp.ReportService;
import pages.base.PageInstance;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import arp.CucumberArpReport;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.setArpReportClient;
import static helpers.SystemHelper.MAINWINDOWHANDLER;
import static helpers.SystemHelper.Reset_Values;

//import pages.LoginPage;

/**
 * Created by logovskoy on 12/3/2015.
 */
public class BeforeAfter extends PageInstance {

    @Before
    public void setUp(Scenario scenario) {
        Reset_Values();
        try {
            setArpReportClient(new CucumberArpReport());
            if (!scenario.getId().startsWith(CucumberArpReport.getTestSuiteName().toLowerCase())) {
                ReportService.close();
                CucumberArpReport.open("B26AAC2A-4D91-4231-9315-9FFA4B6DCD16", scenario.getId());
            }
            CucumberArpReport.addTestToTestSuite(scenario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown(Scenario scenario) throws Exception {
        try {
            CucumberArpReport.decideTestStatus();
            ReportService.FinishTest();
            driver.quit();
            driver = DriverSetup.start();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
}