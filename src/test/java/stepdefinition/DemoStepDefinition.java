package stepdefinition;

import arp.CucumberArpReport;
import arp.ReportService;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.MainMenuPage;
import pages.base.PageInstance;
import pages.AppPage;

/**
 * Created by beckmambetov on 9/26/2016.
 */
public class DemoStepDefinition extends PageInstance {

    //@Autowired
    public MainMenuPage mainMenuPage = new MainMenuPage();

    public AppPage appPage = new AppPage();

    @Given("^I`m on a main page$")
    public void iMOnAMainPage() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(4000);
            ReportService.ReportAction("I`m on a main page", mainMenuPage.isCurrentPage());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I enter \"([^\"]*)\" into search string$")
    public void iEnterIntoSearchString(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            mainMenuPage.search(arg0);
            ReportService.ReportAction("Search was initiated", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see search results and they contain \"([^\"]*)\" in their name$")
    public void iSeeSearchResultsAndTheyContainInTheirName(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.ReportAction("All found results contain value", mainMenuPage.checkSearchResults(arg0));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select \"([^\"]*)\" application$")
    public void iSelectApplication(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            mainMenuPage.goToResult(arg0);
            ReportService.ReportAction("Application was selected", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I install app$")
    public void iInstallApp() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            appPage.installApp();
            ReportService.ReportAction("Application was installed", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that app \"([^\"]*)\" is \"([^\"]*)\" installed$")
    public void iSeeThatAppIsInstalled(String arg0, String arg1) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean result = appPage.isAppInstalled(arg0);
            switch (arg1) {
                case "not":
                    result = !result;
                    break;
                case "":
                    break;
                default:
                    break;
            }
            ReportService.ReportAction("Application state is expected", result);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I delete app$")
    public void iDeleteApp() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            appPage.deleteApp();
            ReportService.ReportAction("Application was deleted", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that it`s average mark is \"([^\"]*)\"$")
    public void iSeeThatItSAverageMarkIs(String arg0) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.ReportAction("Average mark is equal to expected value", arg0.equals(appPage.getAverageMark()));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
